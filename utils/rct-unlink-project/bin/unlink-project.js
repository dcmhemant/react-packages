#!/usr/bin/env node

const debug = require('debug')('react-packages:utils:rct-unlink-project:bin')
const program = require('commander')
const inquirer = require('inquirer')
const lib = require('../')
const { version } = require('../package')

async function start() {
  debug('start()')

  program
    .description('Yarn-unlink all the packages from a project.')
    .usage('--path <PATH>')
    .option('-p, --path <path>', 'the local path to the project repository', '')
    .version(version)
    .parse(process.argv)

  const { path: pPath } = program

  const { iPath } = await inquirer.prompt([
    { name: 'iPath', message: 'Project path?', when: () => !pPath },
  ])

  const projectPath = iPath || pPath

  lib({ projectPath })
}

start()
