const debug = require('debug')('react-packages:utils:rct-unlink-project:lib')
const { execSync } = require('child_process')
const getPackages = require('./_getPackages')

module.exports = ({ projectPath }) => {
  debug(`Remove packages links from the project located at ${projectPath}`)

  getPackages().forEach((p) => {
    debug(`${p}/package.json`)
    // eslint-disable-next-line global-require, import/no-dynamic-require
    const { name } = require(`${p}/package.json`)
    debug(name)
    debug(`Unlink ${name}`)
    execSync(`yarn unlink ${name}`, { stdio: [0, 1, 2], cwd: projectPath })
  })
}
