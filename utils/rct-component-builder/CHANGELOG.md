# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-component-builder

<a name="1.102.1"></a>
## [1.102.1](https://gitlab.com/4geit/react-packages/compare/v1.102.0...v1.102.1) (2018-01-23)


### Bug Fixes

* **utils:** use mustache escape mode ([6e879cf](https://gitlab.com/4geit/react-packages/commit/6e879cf))




<a name="1.102.0"></a>
# [1.102.0](https://gitlab.com/4geit/react-packages/compare/v1.101.4...v1.102.0) (2018-01-23)


### Features

* **utils:** add new option to enable/disable npm organization namespace ([ae4c07a](https://gitlab.com/4geit/react-packages/commit/ae4c07a))




<a name="1.101.4"></a>
## [1.101.4](https://gitlab.com/4geit/react-packages/compare/v1.101.3...v1.101.4) (2018-01-22)


### Bug Fixes

* **utils:** minor ([eadfe38](https://gitlab.com/4geit/react-packages/commit/eadfe38))




<a name="1.101.3"></a>
## [1.101.3](https://gitlab.com/4geit/react-packages/compare/v1.101.2...v1.101.3) (2018-01-13)


### Bug Fixes

* **builders:** replace mustache tags to avoid conflict with JSX tags ([19003b1](https://gitlab.com/4geit/react-packages/commit/19003b1))




<a name="1.101.0"></a>
# [1.101.0](https://gitlab.com/4geit/react-packages/compare/v1.100.1...v1.101.0) (2017-12-19)


### Features

* **utils:** convert scripts to js utils ([cbf6a2b](https://gitlab.com/4geit/react-packages/commit/cbf6a2b))
* **utils:** moved yarn linking related scripts to utils ([111a391](https://gitlab.com/4geit/react-packages/commit/111a391))
