const debug = require('debug')('react-packages:utils:rct-component-builder:lib')
const Mustache = require('mustache')
const fs = require('fs')
const path = require('path')
const gitP = require('simple-git/promise')
const { exec } = require('child_process')
const _ = require('lodash')

async function createDirectories({ directories }) {
  debug('createDirectories()')
  directories.forEach((dir) => {
    const dirPath = `./packages/${dir}`
    debug(dirPath)
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath)
      debug(`the new folder ${dir} has been created!`)
    } else {
      debug(`the folder ${dir} already exists!`)
    }
  })
}
async function createFiles({
  name, type, variables, files,
}) {
  debug('createFiles()')
  files.forEach(({ src, dst }) => {
    debug(src)
    debug(dst)
    const tpl = fs.readFileSync(path.resolve(__dirname, `./template/${src}`), 'utf8')
    Mustache.parse(tpl, ['<(', ')>'])
    const output = Mustache.render(tpl, variables)
    debug(output)
    fs.writeFileSync(`./packages/rct-${name}-${type}/${dst}`, output, 'utf8')
    debug(`the file ${dst} has been created!`)
  })
}
async function prepareRepository({ name, type }) {
  debug('prepareRepository')
  // setup git to working directory
  const git = gitP('.')
  // git add
  await git.add('.')
  // git commit
  await git.commit(`feat(rct-${name}-${type}) new ${type}`)
}
async function yarn() {
  debug('link packages to each other (yarn)')
  await exec('yarn')
}
async function usage({ name, type }) {
  /* eslint-disable no-console */
  console.log()
  console.log()
  console.log('The new package has been properly generated and is available under the packages folder.')
  console.log()
  console.log('You can get access to the generated package with the following command:')
  console.log()
  console.log(`cd packages/rct-${name}-${type}`)
  console.log()
  /* eslint-enable no-console */
}

module.exports = async ({
  organization, group, name, description, authorName, authorEmail, dryRun,
}) => {
  const type = 'component'

  // debug
  debug(`organization: ${organization}`)
  debug(`group: ${group}`)
  debug(`name: ${name}`)
  debug(`type: ${type}`)
  debug(`description: ${description}`)
  debug(`authorName: ${authorName}`)
  debug(`authorEmail: ${authorEmail}`)
  debug(`dryRun: ${dryRun}`)

  // define variables
  const variables = {
    group,
    name,
    description,
    authorName,
    authorEmail,
    type,
    organization: (organization && `@${group}/`) || '',
    escapedOrganization: (organization && `@${group}%2F`) || '',
    _class: _.upperFirst(_.camelCase(`rct-${name}-${type}`)),
    _function: _.camelCase(`rct-${name}-${type}`),
    name_class: _.upperFirst(_.camelCase(name)),
    name_function: _.upperFirst(_.camelCase(name)),
    name_type_class: _.upperFirst(_.camelCase(`${name}-${type}`)),
    name_type_function: _.camelCase(`${name}-${type}`),
  }
  // define files
  const files = [
    { src: '.npmignore', dst: '.npmignore' },
    { src: 'package.json', dst: 'package.json' },
    { src: 'README.md', dst: 'README.md' },
    { src: `src/${type}.css`, dst: `src/rct-${name}.${type}.css` },
    { src: `src/${type}.js`, dst: `src/rct-${name}.${type}.js` },
    { src: `src/${type}.stories.js`, dst: `src/rct-${name}.${type}.stories.js` },
    { src: `src/${type}.test.js`, dst: `src/rct-${name}.${type}.test.js` },
    { src: 'src/index.js', dst: 'src/index.js' },
    { src: 'src/index.ng.js', dst: 'src/index.ng.js' },
  ]
  // define directories
  const directories = [
    `rct-${name}-${type}`,
    `rct-${name}-${type}/src`,
  ]

  // create directories
  await createDirectories({ directories })
  // create files
  await createFiles({
    name, type, variables, files,
  })
  // git stuff
  await prepareRepository({ name, type })
  // yarn
  await yarn()
  // print usage
  await usage({ name, type })
}
