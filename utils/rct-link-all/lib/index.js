const debug = require('debug')('react-packages:utils:rct-link-all:lib')
const { execSync } = require('child_process')
const getPackages = require('./_getPackages')

module.exports = async () => {
  debug('lib')

  getPackages().forEach((p) => {
    debug(`Link ${p}`)
    execSync('yarn link', { stdio: [0, 1, 2], cwd: p })
  })
}
