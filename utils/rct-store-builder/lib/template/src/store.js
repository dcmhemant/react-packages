// eslint-disable-next-line no-unused-vars
import { observable, action, computed } from 'mobx'
import buildDebug from 'debug'

// eslint-disable-next-line no-unused-vars
const debug = buildDebug('react-packages:packages:rct-<(name)>-<(type)>')

export class <(_class)> {
  // @observable var1
  // @observable var2
  // @action setVar1(value) {
  //   debug('setVar1')
  //   this.var1 = value
  // }
  // @action setVar2(value) {
  //   debug('setVar2')
  //   this.var2 = value
  // }
  // @computed get dynamicVar1() {
  //   debug('dynamicVar1')
  //   return this.var1.filter(i => ...)
  // }
}

export default new <(_class)>()
