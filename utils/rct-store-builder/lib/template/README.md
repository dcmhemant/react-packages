# <(&organization)>rct-<(name)>-<(type)> [![npm version](//badge.fury.io/js/<(&escapedOrganization)>rct-<(name)>-<(type)>.svg)](//badge.fury.io/js/<(&escapedOrganization)>rct-<(name)>-<(type)>)

---

<(description)>

## Demo

A live storybook is available to see how the <(type)> looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***<(&organization)>rct-<(name)>-<(type)>*** is through [npm](//www.npmjs.com/search?q=<(&organization)>rct-<(name)>-<(type)>) package manager using the following command:

```bash
npm i <(&organization)>rct-<(name)>-<(type)> --save
```

Or use `yarn` using the following command:

```bash
yarn add <(&organization)>rct-<(name)>-<(type)>
```

2. Depending on where you want to use the <(type)> you will need to import the class instance `<(name_type_function)>` or inject it to your project JS file.

If you are willing to use it within a component, then you must use the `inject` decorator provided by `mobx-react` library.

For instance if you want to use this <(type)> in your `App.js` component, you can use the <(_class)> <(type)> in the JSX code as follows:

```js
import React, { Component } from 'react'
import { inject } from 'mobx-react'
// ...
@inject('<(name_type_function)>')
class App extends Component {
  handleClick() {
    this.props.<(name_type_function)>.setVar1('dummy value')
  }

  render() {
    return (
      <div className="App">
        <button onClick={ this.handleClick.bind(this) } >Click here</button>
      </div>
    )
  }
}
```

If you are willing to use the class instance inside another <(type)> class, then you can just import the instance as follows:

```js
import <(name_type_function)> from '<(&organization)>rct-<(name)>-<(type)>'

class DummyStore {
  @action doSomething() {
    <(name_type_function)>.setVar1('dummy value')
  }
}
```

3. If you want to use the <(type)> class in the storybook, add <(name_type_function)> within `stories/index.js` by first importing it:

```js
import <(name_type_function)> from '<(&organization)>rct-<(name)>-<(type)>'
```

and then within the `stores` array variable add `<(name_type_function)>` at the end of the list.

4. If you want to use the <(type)> class in your project, add <(name_type_function)> within `src/index.js` by first importing it:

```js
import <(name_type_function)> from '<(&organization)>rct-<(name)>-<(type)>'
```

and then within the `stores` array variable add `<(name_type_function)>` at the end of the list.
