// CRA doesnot provide any way to customize its webpack setup withouth entirely
// ejecting the all files, that's why we use `react-app-rewired that allows
// to benefits from CRA updates and still add new custom setup to its webpack
// environment.`

// we import compose in order to compse several rewires
const { compose } = require('react-app-rewired')
// we need mobx rewire to apply the decorator transpilation to babel
const rewireMobX = require('react-app-rewire-mobx')
// we need those for the functions below
const path = require('path')
const fs = require('fs')

// we also want the @4geit/react-packages source files processed so we can
// test the changes of a package on a project in real-time, this variable
// only holds the location of @4geit/react-packages dependencies and
// is used later.
const PACKAGES_DIR = path.resolve(__dirname, './node_modules/@4geit')
const ROOT_DIR = path.resolve(__dirname, './node_modules')

// this function allows to get the list of the real path of @4geit packages
// behind the symlinks since we are using `yarn link` to link the
// `@4geit/react-packages` packages to the project.
function getPackages() {
  return fs
    .readdirSync(PACKAGES_DIR)
    .map(file => fs.realpathSync(path.resolve(PACKAGES_DIR, file)))
}
// get the list of the root `rct-*` packages
function getRootPackages() {
  return fs
    .readdirSync(ROOT_DIR)
    .filter(file => /^rct-/.test(file))
    .map(file => fs.realpathSync(path.resolve(ROOT_DIR, file)))
}

// that's where we start overriding the webpack config of CRA
module.exports = function override(config, env) {
  // we need to extract some fields to override from the webpack config object
  const {
    resolve,
    // eslint-disable-next-line no-unused-vars
    module: { rules: [pre, { oneOf: [image, js] }] },
  } = config
  // this allows to force mobx to use a unique instance
  resolve.alias.mobx = path.resolve('./node_modules/mobx')
  // we add the `src:module` main field since this is the one used by
  // `@4geit/react-packages` to define the source file path
  resolve.mainFields = ['src:module', 'browser', 'module', 'main']
  // we call `getPackages()` and `getRootPackages()` to get the list
  // of @4geit/packages and root packages real path and
  // concat them into the include array
  js.include = getRootPackages().concat(getPackages().concat([js.include]))
  // we compose the rewires that will also override the webpack config and
  // call them.
  const rewires = compose(rewireMobX)
  return rewires(config, env)
}
