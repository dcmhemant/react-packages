import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'

// import xyzStore from '@4geit/rct-xyz-store'

import RctSocialButtonsComponent from './rct-social-buttons.component'

const stores = {
  // xyzStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctSocialButtonsComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider { ...stores } >
      <RctSocialButtonsComponent />
    </Provider>,
    div,
  )
})
// it('renders correctly', () => {
//   const wrapper = mount(<RctSocialButtonsComponent {...stores} />)
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  const wrapper = shallow(<RctSocialButtonsComponent {...stores} />)
  expect(wrapper).toMatchSnapshot()
})
