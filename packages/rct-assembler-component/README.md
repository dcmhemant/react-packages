# @4geit/rct-assembler-component [![npm version](//badge.fury.io/js/@4geit%2Frct-assembler-component.svg)](//badge.fury.io/js/@4geit%2Frct-assembler-component)

---

pick components and assemble them together

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-assembler-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-assembler-component) package manager using the following command:

```bash
npm i @4geit/rct-assembler-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-assembler-component
```

2. Depending on where you want to use the component you will need to import the class `RctAssemblerComponent` to your project JS file as follows:

```js
import RctAssemblerComponent from '@4geit/rct-assembler-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctAssemblerComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctAssemblerComponent from '@4geit/rct-assembler-component'
// ...
const App = () => (
  <div className="App">
    <RctAssemblerComponent />
  </div>
)
```

## AngularJS

`rct-assembler-component` also supports integration in an AngularJS app, therefore you can call the AngularJS component directive into your AngularJS app and it will appears properly, you can have more details about its AngularJS integration [here](./src/index.ng.js).

Here are the steps you need to follow to get the component working on your AngularJS app:

* install the component thanks to npm as shown above
* add the following tag to the main `index.html` file of your app:

```html
<script src="node_modules/@4geit/rct-ng-vendor/dist/index.ng.js"></script>
<script src="node_modules/@4geit/rct-assembler-component/dist/index.ng.js"></script>
```

* inject the component AngularJS module to your app module as below:

```js
angular
  .module('app', ['rct-assembler-component'])
  // ...
```

* use the component directive where ever you want to add the component in your app as follow:

```html
<rct-assembler-component></rct-assembler-component>
```
