# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.86.3"></a>
## [1.86.3](https://gitlab.com/4geit/react-packages/compare/v1.86.2...v1.86.3) (2017-11-21)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.86.1"></a>
## [1.86.1](https://gitlab.com/4geit/react-packages/compare/v1.86.0...v1.86.1) (2017-11-20)


### Bug Fixes

* **notification-menu:** add clearInterval method and remove isAuthenticated method ([e26722a](https://gitlab.com/4geit/react-packages/commit/e26722a))




<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)


### Bug Fixes

* **notification-menu-component:** fix ([c986f55](https://gitlab.com/4geit/react-packages/commit/c986f55))
* **rct-notitication-menu:** handle different actions on notifications ([9f3c981](https://gitlab.com/4geit/react-packages/commit/9f3c981))




<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.83.17"></a>
## [1.83.17](https://gitlab.com/4geit/react-packages/compare/v1.83.16...v1.83.17) (2017-11-10)


### Bug Fixes

* **notification-menu-component:** add list subheader and fix close button issue ([519e05b](https://gitlab.com/4geit/react-packages/commit/519e05b))




<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.83.1"></a>
## [1.83.1](https://gitlab.com/4geit/react-packages/compare/v1.83.0...v1.83.1) (2017-11-03)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-notification-menu-store

<a name="1.80.1"></a>
## [1.80.1](https://gitlab.com/4geit/react-packages/compare/v1.80.0...v1.80.1) (2017-10-28)


### Bug Fixes

* **notification-menu:** fix ui issues ([35931a7](https://gitlab.com/4geit/react-packages/commit/35931a7))




<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.77.0"></a>
# [1.77.0](https://gitlab.com/4geit/react-packages/compare/v1.76.1...v1.77.0) (2017-10-21)


### Bug Fixes

* **minor:** minor ([ad13041](https://gitlab.com/4geit/react-packages/commit/ad13041))
* **notification-menu:** changes to icon approach and fix issues in typeMapping ([cecf6ee](https://gitlab.com/4geit/react-packages/commit/cecf6ee))
* **notification-menu:** define typeIcons, typePhrases and typeMapping as member of the store class ([d56cf91](https://gitlab.com/4geit/react-packages/commit/d56cf91))




<a name="1.76.1"></a>
## [1.76.1](https://gitlab.com/4geit/react-packages/compare/v1.76.0...v1.76.1) (2017-10-20)


### Bug Fixes

* **notification-menu-store:** remove unnecessary mapping ([83b6301](https://gitlab.com/4geit/react-packages/commit/83b6301))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Bug Fixes

* **storybook:** store user info ([4a39bc3](https://gitlab.com/4geit/react-packages/commit/4a39bc3))




<a name="1.69.1"></a>
## [1.69.1](https://gitlab.com/4geit/react-packages/compare/v1.69.0...v1.69.1) (2017-10-10)


### Bug Fixes

* **minor:** minor ([771fc11](https://gitlab.com/4geit/react-packages/commit/771fc11))
* **notification-menu-component:** fix UI issues ([66d5db9](https://gitlab.com/4geit/react-packages/commit/66d5db9))




<a name="1.67.1"></a>
## [1.67.1](https://gitlab.com/4geit/react-packages/compare/v1.67.0...v1.67.1) (2017-10-09)


### Bug Fixes

* **notification-menu-store:** activate fetchData method to use mock api, change avator icon logic ([e712899](https://gitlab.com/4geit/react-packages/commit/e712899))
* **notification-menu-store:** changes to data structure ([cbc9ef5](https://gitlab.com/4geit/react-packages/commit/cbc9ef5))




<a name="1.63.0"></a>
# [1.63.0](https://gitlab.com/4geit/react-packages/compare/v1.62.1...v1.63.0) (2017-10-06)


### Bug Fixes

* **minor:** minor ([9f7a286](https://gitlab.com/4geit/react-packages/commit/9f7a286))


### Features

* **notification-menu-store:** add pagination to load more button and progress circular ([d9984b0](https://gitlab.com/4geit/react-packages/commit/d9984b0))




<a name="1.62.1"></a>
## [1.62.1](https://gitlab.com/4geit/react-packages/compare/v1.62.0...v1.62.1) (2017-10-06)


### Bug Fixes

* **notification-menu-component:** fix horizontal scroll and set id to have unique prop key ([eaedda7](https://gitlab.com/4geit/react-packages/commit/eaedda7))




<a name="1.59.0"></a>
# [1.59.0](https://gitlab.com/4geit/react-packages/compare/v1.58.1...v1.59.0) (2017-10-05)


### Features

* **rct-notification-menu-component:** add badge as well as avatar icon ([7af3075](https://gitlab.com/4geit/react-packages/commit/7af3075))




<a name="1.58.1"></a>
## [1.58.1](https://gitlab.com/4geit/react-packages/compare/v1.58.0...v1.58.1) (2017-10-05)


### Bug Fixes

* **notification-menu-component:** fixed notification content ([b92b0ed](https://gitlab.com/4geit/react-packages/commit/b92b0ed))




<a name="1.52.1"></a>
## [1.52.1](https://gitlab.com/4geit/react-packages/compare/v1.52.0...v1.52.1) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add isRead logic for notification background ([7dc67a8](https://gitlab.com/4geit/react-packages/commit/7dc67a8))




<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.51.0"></a>
# [1.51.0](https://gitlab.com/4geit/react-packages/compare/v1.50.0...v1.51.0) (2017-10-04)


### Features

* **notification-menu-store:** create notification menu store ([7d53464](https://gitlab.com/4geit/react-packages/commit/7d53464))
