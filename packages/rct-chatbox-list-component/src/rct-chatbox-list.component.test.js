import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import chatboxListStore from '@4geit/rct-chatbox-list-store'
import chatboxGridStore from '@4geit/rct-chatbox-grid-store'

import RctChatboxListComponent from './rct-chatbox-list.component'

const debug = buildDebug('react-packages:packages:test:rct-chatbox-list-component')

const stores = {
  commonStore,
  swaggerClientStore,
  chatboxListStore,
  chatboxGridStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctChatboxListComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores} >
      <RctChatboxListComponent />
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <RctChatboxListComponent {...stores} />
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctChatboxListComponent {...stores} />)
  expect(wrapper).toMatchSnapshot()
})
