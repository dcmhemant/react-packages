import React from 'react'
import angular from 'angular'
import { react2angular } from 'react2angular'
import { Provider } from 'mobx-react'

import RctTodoListComponent from './rct-todo-list.component'

const stores = {
  // TBD
}

const RctTodoListComponentWrapper = () => (
  <Provider {...stores} >
    <RctTodoListComponent />
  </Provider>
)

export default angular
  .module('rct-todo-list-component', [])
  .component('rctTodoListComponent', react2angular(RctTodoListComponentWrapper))
  .name
