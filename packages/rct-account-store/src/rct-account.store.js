import { observable, action, runInAction } from 'mobx'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

const debug = buildDebug('react-packages:packages:rct-account-store')

export class RctAccountStore {
  @observable body = {}

  @action setBody(value) {
    debug('setBody()')
    this.body = value
  }
  @action setAddressField(value, key) {
    debug('setAddressField()')
    this.body.address[key] = value
  }
  @action setBodyField(key, value) {
    debug('setBodyField()')
    this.body[key] = value
  }
  @action async userUpdate({ updateOperationId }) {
    debug('userUpdate()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[updateOperationId || 'userUpdate']({
        id: commonStore.user.id,
        body: this.body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctAccountStore()
