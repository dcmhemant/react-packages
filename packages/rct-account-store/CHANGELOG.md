# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.83.4"></a>
## [1.83.4](https://gitlab.com/4geit/react-packages/compare/v1.83.3...v1.83.4) (2017-11-06)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.82.5"></a>
## [1.82.5](https://gitlab.com/4geit/react-packages/compare/v1.82.4...v1.82.5) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)


### Bug Fixes

* **rct-account-store:** minor fix ([ccde80e](https://gitlab.com/4geit/react-packages/commit/ccde80e))




<a name="1.80.3"></a>
## [1.80.3](https://gitlab.com/4geit/react-packages/compare/v1.80.2...v1.80.3) (2017-10-30)


### Bug Fixes

* **account-component:** fix debug issue ([001501a](https://gitlab.com/4geit/react-packages/commit/001501a))




<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-account-store

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.76.0"></a>
# [1.76.0](https://gitlab.com/4geit/react-packages/compare/v1.75.2...v1.76.0) (2017-10-19)


### Bug Fixes

* **account store:** handle on change ([200b3ce](https://gitlab.com/4geit/react-packages/commit/200b3ce))
* **account store:** minor fix ([1a24fbc](https://gitlab.com/4geit/react-packages/commit/1a24fbc))
* **account store:** minor fix ([8c83937](https://gitlab.com/4geit/react-packages/commit/8c83937))


### Features

* **Account store:** Add Update method ([c175cfc](https://gitlab.com/4geit/react-packages/commit/c175cfc))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Features

* **Account Store:** create fetchData method to get user info ([461c460](https://gitlab.com/4geit/react-packages/commit/461c460))




<a name="1.74.0"></a>
# [1.74.0](https://gitlab.com/4geit/react-packages/compare/v1.73.0...v1.74.0) (2017-10-12)


### Features

* **AccountBox:** Add user textfields ([67fb588](https://gitlab.com/4geit/react-packages/commit/67fb588))




<a name="1.73.0"></a>
# [1.73.0](https://gitlab.com/4geit/react-packages/compare/v1.72.0...v1.73.0) (2017-10-12)


### Features

* **account store:** create package account store ([44f1e7c](https://gitlab.com/4geit/react-packages/commit/44f1e7c))
