import { observable, action, runInAction } from 'mobx'
import buildDebug from 'debug'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

const debug = buildDebug('react-packages:packages:rct-todo-store')

export class RctTodoStore {
  @observable todoList = []

  @action setTodoList(value) {
    debug('setTodoList()')
    this.todoList = value
  }
  @action async fetchData({ listOperationId }) {
    debug('fetchData()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId || 'todoList']()
      if (body.length) {
        runInAction(() => {
          this.setTodoList(body)
          this.inProgress = false
        })
      }
    } catch (err) {
      debug(err)
      runInAction(async () => {
        await notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctTodoStore()
