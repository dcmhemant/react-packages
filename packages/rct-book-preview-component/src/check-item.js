import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { withStyles } from 'material-ui/styles'
import Checkbox from 'material-ui/Checkbox'
import { FormControlLabel } from 'material-ui/Form'
import { ListItem } from 'material-ui/List'

const debug = buildDebug('react-packages:packages:rct-book-preview-component:check-item')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
}))
// eslint-disable-next-line react/prefer-stateless-function
export default class CheckItem extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
  }
  render() {
    debug('render()')
    const { label } = this.props
    return (
      <ListItem dense>
        <FormControlLabel
          control={<Checkbox checked />}
          label={label}
        />
      </ListItem>
    )
  }
}
