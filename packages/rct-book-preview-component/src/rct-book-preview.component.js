import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import List from 'material-ui/List'

import './rct-book-preview.component.css'

const debug = buildDebug('react-packages:packages:rct-book-preview-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({

}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctBookPreviewComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
    button: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    slogan: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    fbCover: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    linkToSample: PropTypes.string,
  }
  static defaultProps = {
    linkToSample: '#sample',
    // TBD
  }

  render() {
    debug('render()')
    const {
      children, fbCover, title, slogan, button, linkToSample,
    } = this.props
    return (
      // general layout
      <Grid container direction="row">
        {/* left part layout */}
        <Grid item xs>
          {/* sub left part */}
          <Grid container direction="column" spacing={24}>
            {/* title */}
            <Grid item>
              <Typography type="display3">
                { title }
              </Typography>
            </Grid>
            {/* !title */}
            {/* slogan */}
            <Grid item>
              <Typography type="title">
                { slogan }
              </Typography>
            </Grid>
            {/* !slogan */}
            {/* order button */}
            <Grid item>
              <Grid container direction="column" justify="center" alignItems="center">
                <Grid item>
                  <Button raised color="contrast">{ button }</Button>
                </Grid>
                <Grid item>
                  <Button href={linkToSample} color="contrast">En savoir +</Button>
                </Grid>
              </Grid>
            </Grid>
            {/* !order button */}
            {/* checklist part */}
            <Grid item>
              <List dense>
                {children}
              </List>
            </Grid>
            {/* checklist part */}
          </Grid>
          {/* sub left part */}
        </Grid>
        {/* left part layout */}
        {/* right part layout */}
        <Grid item xs>
          {/* sub right part */}
          <Grid container direction="column">
            {/* book picture */}
            <Grid item>
              <img
                style={{
                  paddingBottom: 20,
                  paddingLeft: 60,
                  height: 600,
                }}
                src={fbCover}
                alt=""
              />
            </Grid>
            {/* !book picture */}
          </Grid>
          {/* !sub right part */}
        </Grid>
        {/* !right part layout */ }
      </Grid>
      // !general layout
    )
  }
}
