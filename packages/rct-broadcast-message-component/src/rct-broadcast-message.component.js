import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import TextField from 'material-ui/TextField'
import Card, { CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'

import './rct-broadcast-message.component.css'

const debug = buildDebug('react-packages:packages:rct-broadcast-message-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  form: {
    width: '100%',
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctBroadcastMessageComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    helper: PropTypes.string,
    label: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }
  static defaultProps = {
    helper: 'Send a message',
  }

  @observable message = ''

  @action handleMessageChange = (event) => {
    debug('handleMessageChange()')
    this.message = event.target.value
  }
  @action handleOnSubmit = (event) => {
    debug('handleOnSubmit()')
    event.preventDefault()
    const { onSubmit } = this.props
    onSubmit(this.message)
    this.message = ''
  }

  render() {
    debug('render()')
    const {
      classes, helper, label,
    } = this.props
    return (
      <div>
        <form className={classes.form} onSubmit={this.handleOnSubmit}>
          <Card>
            <CardContent>
              <Grid container alignItems="center" justify="space-between">
                {/* message field */}
                <Grid item xs={11}>
                  <TextField
                    label={label}
                    helperText={helper}
                    fullWidth
                    margin="normal"
                    value={this.message}
                    onChange={this.handleMessageChange}
                  />
                </Grid>
                {/* send button */}
                <Grid item xs={1}>
                  <Grid container justify="center">
                    <Grid item>
                      <Button dense raised color="primary" type="submit">Send</Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </form>
      </div>
    )
  }
}
