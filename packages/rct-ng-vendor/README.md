# @4geit/rct-ng-vendor [![npm version](//badge.fury.io/js/@4geit%2Frct-ng-vendor.svg)](//badge.fury.io/js/@4geit%2Frct-ng-vendor)

---

This package gathers all the common vendor modules so we avoid duplication of dependencies when loading several components into an old web application architecture that does not support import/export module feature.

## Installation

1. A recommended way to install ***@4geit/rct-ng-vendor*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-ng-vendor) package manager using the following command:

```bash
npm install @4geit/rct-ng-vendor
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-ng-vendor
```
