import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import './rct-author.component.css'

const debug = buildDebug('react-packages:packages:rct-author-component')


// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctAuthorComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    authorPicture: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    // TBD
  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const {
      authorPicture, children, title, author,
    } = this.props
    return (
      <Grid container direction="row" spacing={40}>
        {/* picture */}
        <Grid item>
          <img src={authorPicture} alt="" />
        </Grid>
        {/* !picture */}
        {/* bio */}
        <Grid item xs>
          <Grid container direction="column" justify="center" alignItems="center" style={{ height: '100%' }}>
            <Grid item>
              <Typography type="headline" gutterBottom>
                { title }
              </Typography>
              { children }
              <Typography type="subheading" align="right" paragraph>
                { author }
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        {/* !bio */}
      </Grid>
    )
  }
}
