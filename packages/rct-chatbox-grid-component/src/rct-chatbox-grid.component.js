import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types';
import buildDebug from 'debug'
// eslint-disable-next-line
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
// eslint-disable-next-line
import { withStyles } from 'material-ui/styles'
// eslint-disable-next-line
import withWidth from 'material-ui/utils/withWidth'
// import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import { CircularProgress } from 'material-ui/Progress'

import { RctChatboxGridStore } from '@4geit/rct-chatbox-grid-store'

import RctReorderableGridListComponent from '@4geit/rct-reorderable-grid-list-component'
import RctBroadcastMessageComponent from '@4geit/rct-broadcast-message-component'

import ItemComponent from './item.component'
import MaximizeComponent from './maximize.component'

import './rct-chatbox-grid.component.css'

const debug = buildDebug('react-packages:packages:rct-chatbox-grid-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  root: {
    margin: 'auto',
    height: '100vh',
  },
  // TBD
}))
@withWidth()
@inject('chatboxGridStore')
@observer
export default class RctChatboxGridComponent extends Component {
  static propTypes = {
    classes: PropTypes.shape({}).isRequired,
    chatboxGridStore: PropTypes.instanceOf(RctChatboxGridStore).isRequired,
    listOperationId: PropTypes.string,
    updateOperationId: PropTypes.string,
    deleteOperationId: PropTypes.string,
  }
  static defaultProps = {
    listOperationId: 'userChatboxList',
    updateOperationId: 'userChatboxUpdate',
    deleteOperationId: 'userChatboxDelete',
  }

  async componentWillMount() {
    debug('componentWillMount()')
    const { listOperationId, chatboxGridStore } = this.props
    await chatboxGridStore.fetchMaximizedItem({ listOperationId })
    await chatboxGridStore.fetchData({ listOperationId })
  }

  handleSubmitMessage = async (message) => {
    debug('handleSubmitMessage')
    debug(message)
    const { chatboxGridStore } = this.props
    await chatboxGridStore.sendMessage({ message, author: 'Dummy Author' })
    await chatboxGridStore.fetchMaximizedItem({ listOperationId: 'userChatboxList' })
    await chatboxGridStore.fetchData({ listOperationId: 'userChatboxList' })
  }

  render() {
    debug('render()')
    const {
      classes, chatboxGridStore, updateOperationId, listOperationId,
      deleteOperationId,
    } = this.props
    const {
      fetchData, setPosition, inProgress,
      maximizedItem,
    } = chatboxGridStore

    if (maximizedItem) {
      return (
        <div>
          <MaximizeComponent
            updateOperationId={updateOperationId}
            listOperationId={listOperationId}
            deleteOperationId={deleteOperationId}
            onMessageSubmit={this.handleSubmitMessage}
          />
        </div>
      )
    }

    if (inProgress) {
      return (
        <Grid container justify="center" alignItems="center" className={classes.root}>
          <Grid item xs style={{ textAlign: 'center' }}>
            <CircularProgress />
          </Grid>
        </Grid>
      )
    }

    /* eslint-disable react/jsx-no-bind */
    return (
      <div>
        <RctReorderableGridListComponent
          cellHeight={180}
          spacing={16}
          className={classes.gridList}
          handleSetPosition={setPosition.bind(chatboxGridStore)}
          handleFetchData={fetchData.bind(chatboxGridStore)}
          updateOperationId={updateOperationId}
          listOperationId={listOperationId}
          deleteOperationId={deleteOperationId}
          data={chatboxGridStore.sortedData}
          itemComponent={ItemComponent}
          cols={4}
          itemWidth={340}
        />
        <br />
        <RctBroadcastMessageComponent
          label="Send a message to all"
          onSubmit={this.handleSubmitMessage}
        />
      </div>
    )
    /* eslint-enable react/jsx-no-bind */
  }
}
