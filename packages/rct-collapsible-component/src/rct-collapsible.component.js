import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel'
import ExpandMoreIcon from 'material-ui-icons/ExpandMore'
import Divider from 'material-ui/Divider'

const debug = buildDebug('react-packages:packages:rct-collapsible-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctCollapsibleComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,

  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const { title, children } = this.props
    return (
      <div>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            {title}
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {children}
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <Divider />
      </div>
    )
  }
}
