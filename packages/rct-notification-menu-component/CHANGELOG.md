# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.2"></a>
## [1.103.2](https://gitlab.com/4geit/react-packages/compare/v1.103.1...v1.103.2) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.87.4"></a>
## [1.87.4](https://gitlab.com/4geit/react-packages/compare/v1.87.3...v1.87.4) (2017-11-24)


### Bug Fixes

* **rct-notification-menu-component:** remove payment method when subtype updated ([6a27e0d](https://gitlab.com/4geit/react-packages/commit/6a27e0d))




<a name="1.87.2"></a>
## [1.87.2](https://gitlab.com/4geit/react-packages/compare/v1.87.1...v1.87.2) (2017-11-23)


### Bug Fixes

* **notification-menu:** minor ([9dbb8a4](https://gitlab.com/4geit/react-packages/commit/9dbb8a4))




<a name="1.87.1"></a>
## [1.87.1](https://gitlab.com/4geit/react-packages/compare/v1.87.0...v1.87.1) (2017-11-23)


### Bug Fixes

* **rct-notification-menu-component:** reset tinyicon when notifications are read ([491f0d4](https://gitlab.com/4geit/react-packages/commit/491f0d4))




<a name="1.87.0"></a>
# [1.87.0](https://gitlab.com/4geit/react-packages/compare/v1.86.5...v1.87.0) (2017-11-22)


### Features

* **rct-notification-menu-component:** install tinycon feature ([bc44a38](https://gitlab.com/4geit/react-packages/commit/bc44a38))




<a name="1.86.5"></a>
## [1.86.5](https://gitlab.com/4geit/react-packages/compare/v1.86.4...v1.86.5) (2017-11-21)


### Bug Fixes

* **rct-notification-menu-component:** fix cancellation wording issue ([081d35f](https://gitlab.com/4geit/react-packages/commit/081d35f))




<a name="1.86.4"></a>
## [1.86.4](https://gitlab.com/4geit/react-packages/compare/v1.86.3...v1.86.4) (2017-11-21)


### Bug Fixes

* **rct-notification-menu-component:** fix load more issue by removing condition to open menu ([88f7e52](https://gitlab.com/4geit/react-packages/commit/88f7e52))




<a name="1.86.3"></a>
## [1.86.3](https://gitlab.com/4geit/react-packages/compare/v1.86.2...v1.86.3) (2017-11-21)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.86.1"></a>
## [1.86.1](https://gitlab.com/4geit/react-packages/compare/v1.86.0...v1.86.1) (2017-11-20)


### Bug Fixes

* **notification-menu:** add clearInterval method and remove isAuthenticated method ([e26722a](https://gitlab.com/4geit/react-packages/commit/e26722a))




<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)


### Bug Fixes

* **notification-menu-component:** fix ([c986f55](https://gitlab.com/4geit/react-packages/commit/c986f55))
* **rct-notification-menu-component:** add method to check if user is authenticated ([e69204e](https://gitlab.com/4geit/react-packages/commit/e69204e))
* **rct-notification-menu-component:** fix merge conflicts errors ([e3cb5e9](https://gitlab.com/4geit/react-packages/commit/e3cb5e9))
* **rct-notitication-menu:** handle different actions on notifications ([9f3c981](https://gitlab.com/4geit/react-packages/commit/9f3c981))




<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.83.17"></a>
## [1.83.17](https://gitlab.com/4geit/react-packages/compare/v1.83.16...v1.83.17) (2017-11-10)


### Bug Fixes

* **notification-menu-component:** add list subheader and fix close button issue ([519e05b](https://gitlab.com/4geit/react-packages/commit/519e05b))




<a name="1.83.16"></a>
## [1.83.16](https://gitlab.com/4geit/react-packages/compare/v1.83.15...v1.83.16) (2017-11-09)


### Bug Fixes

* **notification-menu-component:** add close button in menu ([4372470](https://gitlab.com/4geit/react-packages/commit/4372470))




<a name="1.83.15"></a>
## [1.83.15](https://gitlab.com/4geit/react-packages/compare/v1.83.14...v1.83.15) (2017-11-08)


### Bug Fixes

* **notification-menu-component:** use correct badge color ([12f7726](https://gitlab.com/4geit/react-packages/commit/12f7726))




<a name="1.83.14"></a>
## [1.83.14](https://gitlab.com/4geit/react-packages/compare/v1.83.13...v1.83.14) (2017-11-07)


### Bug Fixes

* **notification-menu-component:** fix badge color, selected first menu item, and badge loading inter ([d18d2ef](https://gitlab.com/4geit/react-packages/commit/d18d2ef))




<a name="1.83.13"></a>
## [1.83.13](https://gitlab.com/4geit/react-packages/compare/v1.83.12...v1.83.13) (2017-11-07)


### Bug Fixes

* **rct-notification-menu-component:** add setInterval function to getHeaders call ([ce70c99](https://gitlab.com/4geit/react-packages/commit/ce70c99))




<a name="1.83.12"></a>
## [1.83.12](https://gitlab.com/4geit/react-packages/compare/v1.83.11...v1.83.12) (2017-11-07)


### Bug Fixes

* **minor:** minor ([ed93059](https://gitlab.com/4geit/react-packages/commit/ed93059))
* **notification-menu-component:** add abl orange to badge color, horizontally centered view buttons ([94573f8](https://gitlab.com/4geit/react-packages/commit/94573f8))




<a name="1.83.11"></a>
## [1.83.11](https://gitlab.com/4geit/react-packages/compare/v1.83.10...v1.83.11) (2017-11-07)


### Bug Fixes

* **notification-menu-component:** add clientId parameter ([6611261](https://gitlab.com/4geit/react-packages/commit/6611261))




<a name="1.83.10"></a>
## [1.83.10](https://gitlab.com/4geit/react-packages/compare/v1.83.9...v1.83.10) (2017-11-07)


### Bug Fixes

* **notification-menu-component:** add redirect link to names, fix badge and icon position ([be9e6f4](https://gitlab.com/4geit/react-packages/commit/be9e6f4))




<a name="1.83.9"></a>
## [1.83.9](https://gitlab.com/4geit/react-packages/compare/v1.83.8...v1.83.9) (2017-11-07)


### Bug Fixes

* **notification-menu-component:** change notification menu icon ([7e12a83](https://gitlab.com/4geit/react-packages/commit/7e12a83))




<a name="1.83.7"></a>
## [1.83.7](https://gitlab.com/4geit/react-packages/compare/v1.83.6...v1.83.7) (2017-11-06)


### Bug Fixes

* **notification-menu-component:** add handcurso on notification menu avatar ([1eacd77](https://gitlab.com/4geit/react-packages/commit/1eacd77))




<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.83.1"></a>
## [1.83.1](https://gitlab.com/4geit/react-packages/compare/v1.83.0...v1.83.1) (2017-11-03)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.82.7"></a>
## [1.82.7](https://gitlab.com/4geit/react-packages/compare/v1.82.6...v1.82.7) (2017-11-02)


### Bug Fixes

* **notification-menu-component:** reset page to 1 on handleClick ([f7e092e](https://gitlab.com/4geit/react-packages/commit/f7e092e))




<a name="1.82.6"></a>
## [1.82.6](https://gitlab.com/4geit/react-packages/compare/v1.82.5...v1.82.6) (2017-11-02)


### Bug Fixes

* **rct-notification-component:** fix error with redirect link ([305ef67](https://gitlab.com/4geit/react-packages/commit/305ef67))




<a name="1.82.5"></a>
## [1.82.5](https://gitlab.com/4geit/react-packages/compare/v1.82.4...v1.82.5) (2017-11-02)


### Bug Fixes

* **notification-menu-component:** change history to location for redirection ([f847823](https://gitlab.com/4geit/react-packages/commit/f847823))




<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.82.3"></a>
## [1.82.3](https://gitlab.com/4geit/react-packages/compare/v1.82.2...v1.82.3) (2017-11-01)


### Bug Fixes

* **notification-menu-component:** add withRouter to notification menu component ([0d6d1a9](https://gitlab.com/4geit/react-packages/commit/0d6d1a9))




<a name="1.81.3"></a>
## [1.81.3](https://gitlab.com/4geit/react-packages/compare/v1.81.2...v1.81.3) (2017-11-01)


### Bug Fixes

* **notification-menu-component:** minor style changes ([77d3459](https://gitlab.com/4geit/react-packages/commit/77d3459))




<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.81.1"></a>
## [1.81.1](https://gitlab.com/4geit/react-packages/compare/v1.81.0...v1.81.1) (2017-10-31)


### Bug Fixes

* **notification-menu-component:** changes following to changes in notification endpoints ([eab962f](https://gitlab.com/4geit/react-packages/commit/eab962f))




<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.80.1"></a>
## [1.80.1](https://gitlab.com/4geit/react-packages/compare/v1.80.0...v1.80.1) (2017-10-28)


### Bug Fixes

* **notification-menu:** fix ui issues ([35931a7](https://gitlab.com/4geit/react-packages/commit/35931a7))




<a name="1.79.2"></a>
## [1.79.2](https://gitlab.com/4geit/react-packages/compare/v1.79.1...v1.79.2) (2017-10-25)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.79.1"></a>
## [1.79.1](https://gitlab.com/4geit/react-packages/compare/v1.79.0...v1.79.1) (2017-10-24)


### Bug Fixes

* **notification-menu-component:** fix badge and notification menu size issue ([9794f91](https://gitlab.com/4geit/react-packages/commit/9794f91))




<a name="1.79.0"></a>
# [1.79.0](https://gitlab.com/4geit/react-packages/compare/v1.78.2...v1.79.0) (2017-10-24)


### Bug Fixes

* **notification-menu-component:** made avatar smaller ([6a437b4](https://gitlab.com/4geit/react-packages/commit/6a437b4))




<a name="1.78.2"></a>
## [1.78.2](https://gitlab.com/4geit/react-packages/compare/v1.78.1...v1.78.2) (2017-10-23)


### Bug Fixes

* **notification-menu-component:** change _id to id ([83e2d37](https://gitlab.com/4geit/react-packages/commit/83e2d37))




<a name="1.78.1"></a>
## [1.78.1](https://gitlab.com/4geit/react-packages/compare/v1.78.0...v1.78.1) (2017-10-23)


### Bug Fixes

* **minor:** minor ([c1e4bd8](https://gitlab.com/4geit/react-packages/commit/c1e4bd8))
* **minor:** minor async method ([b472689](https://gitlab.com/4geit/react-packages/commit/b472689))
* **minor:** typo error ([18016cd](https://gitlab.com/4geit/react-packages/commit/18016cd))
* **notification-menu-component:** add handleNotificationMethod and onClick to button ([eced41e](https://gitlab.com/4geit/react-packages/commit/eced41e))




<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.77.0"></a>
# [1.77.0](https://gitlab.com/4geit/react-packages/compare/v1.76.1...v1.77.0) (2017-10-21)


### Bug Fixes

* **minor:** minor ([ad13041](https://gitlab.com/4geit/react-packages/commit/ad13041))
* **notification-menu:** changes to icon approach and fix issues in typeMapping ([cecf6ee](https://gitlab.com/4geit/react-packages/commit/cecf6ee))
* **notification-menu:** define typeIcons, typePhrases and typeMapping as member of the store class ([d56cf91](https://gitlab.com/4geit/react-packages/commit/d56cf91))




<a name="1.76.1"></a>
## [1.76.1](https://gitlab.com/4geit/react-packages/compare/v1.76.0...v1.76.1) (2017-10-20)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.70.2"></a>
## [1.70.2](https://gitlab.com/4geit/react-packages/compare/v1.70.1...v1.70.2) (2017-10-11)


### Bug Fixes

* **package.json:** add moment dependency ([999ccdf](https://gitlab.com/4geit/react-packages/commit/999ccdf))




<a name="1.69.1"></a>
## [1.69.1](https://gitlab.com/4geit/react-packages/compare/v1.69.0...v1.69.1) (2017-10-10)


### Bug Fixes

* **minor:** minor ([6088816](https://gitlab.com/4geit/react-packages/commit/6088816))
* **minor:** minor ([771fc11](https://gitlab.com/4geit/react-packages/commit/771fc11))
* **notification-menu-component:** fix UI issues ([66d5db9](https://gitlab.com/4geit/react-packages/commit/66d5db9))




<a name="1.67.1"></a>
## [1.67.1](https://gitlab.com/4geit/react-packages/compare/v1.67.0...v1.67.1) (2017-10-09)


### Bug Fixes

* **minor:** minor ([ff96e16](https://gitlab.com/4geit/react-packages/commit/ff96e16))
* **notification-menu-store:** activate fetchData method to use mock api, change avator icon logic ([e712899](https://gitlab.com/4geit/react-packages/commit/e712899))
* **notification-menu-store:** changes to data structure ([cbc9ef5](https://gitlab.com/4geit/react-packages/commit/cbc9ef5))




<a name="1.63.1"></a>
## [1.63.1](https://gitlab.com/4geit/react-packages/compare/v1.63.0...v1.63.1) (2017-10-06)


### Bug Fixes

* **minor:** minor ([313f3bf](https://gitlab.com/4geit/react-packages/commit/313f3bf))
* **notification-menu-component:** add path prop to see all button ([bed769d](https://gitlab.com/4geit/react-packages/commit/bed769d))




<a name="1.63.0"></a>
# [1.63.0](https://gitlab.com/4geit/react-packages/compare/v1.62.1...v1.63.0) (2017-10-06)


### Features

* **notification-menu-store:** add pagination to load more button and progress circular ([d9984b0](https://gitlab.com/4geit/react-packages/commit/d9984b0))




<a name="1.62.1"></a>
## [1.62.1](https://gitlab.com/4geit/react-packages/compare/v1.62.0...v1.62.1) (2017-10-06)


### Bug Fixes

* **notification-menu-component:** fix horizontal scroll and set id to have unique prop key ([eaedda7](https://gitlab.com/4geit/react-packages/commit/eaedda7))




<a name="1.59.0"></a>
# [1.59.0](https://gitlab.com/4geit/react-packages/compare/v1.58.1...v1.59.0) (2017-10-05)


### Features

* **rct-notification-menu-component:** add badge as well as avatar icon ([7af3075](https://gitlab.com/4geit/react-packages/commit/7af3075))




<a name="1.58.1"></a>
## [1.58.1](https://gitlab.com/4geit/react-packages/compare/v1.58.0...v1.58.1) (2017-10-05)


### Bug Fixes

* **minor:** minor ([f7b9fa0](https://gitlab.com/4geit/react-packages/commit/f7b9fa0))
* **notification-menu-component:** fixed notification content ([b92b0ed](https://gitlab.com/4geit/react-packages/commit/b92b0ed))




<a name="1.52.1"></a>
## [1.52.1](https://gitlab.com/4geit/react-packages/compare/v1.52.0...v1.52.1) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add isRead logic for notification background ([7dc67a8](https://gitlab.com/4geit/react-packages/commit/7dc67a8))
* **notification-menu-component:** fix anchorEl issue ([74dd0e1](https://gitlab.com/4geit/react-packages/commit/74dd0e1))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.51.2"></a>
## [1.51.2](https://gitlab.com/4geit/react-packages/compare/v1.51.1...v1.51.2) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** fix binding issue ([57b8bab](https://gitlab.com/4geit/react-packages/commit/57b8bab))




<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.50.0"></a>
# [1.50.0](https://gitlab.com/4geit/react-packages/compare/v1.49.0...v1.50.0) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add prop to define operationId to call endpoint ([be80f7e](https://gitlab.com/4geit/react-packages/commit/be80f7e))




<a name="1.48.2"></a>
## [1.48.2](https://gitlab.com/4geit/react-packages/compare/v1.48.1...v1.48.2) (2017-10-03)


### Bug Fixes

* **notification-menu-component:** add notification button, menu component and content ([731bcc6](https://gitlab.com/4geit/react-packages/commit/731bcc6))
* **notification-menu-component:** minor ([66fed9e](https://gitlab.com/4geit/react-packages/commit/66fed9e))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)


### Features

* **notification-menu-component:** create notification menu componenet and added it to storybook ([73b44e2](https://gitlab.com/4geit/react-packages/commit/73b44e2))
