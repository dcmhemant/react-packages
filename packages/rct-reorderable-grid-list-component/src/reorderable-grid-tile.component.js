import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { DragSource, DropTarget } from 'react-dnd'

import { GridListTile } from 'material-ui/GridList'

const debug = buildDebug('react-packages:packages:rct-reorderable-grid-list-component:reorderable-grid-tile')

@DragSource('tile', {
  beginDrag(props) {
    debug('beginDrag()')
    return { ...props }
  },
}, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
}))
@DropTarget('tile', {
  drop(props, monitor, component) {
    debug('drop()')
    const source = monitor.getItem()
    const target = props
    const {
      props: {
        handleSetPosition, handleFetchData, updateOperationId, listOperationId,
      },
    } = component
    if (monitor.didDrop) {
      Promise.resolve()
        // update position of first item
        .then(() => handleSetPosition({
          updateOperationId,
          itemId: source.itemId,
          position: target.position,
        }))
        // update position of second item
        .then(() => handleSetPosition({
          updateOperationId,
          itemId: target.itemId,
          position: source.position < target.position ? target.position - 1 : target.position + 1,
        }))
        // re-fetch data from server
        .then(() => handleFetchData({ listOperationId }))
    }
  },
}, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
}))
export default class ReorderableGridTileComponent extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    isOver: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    itemId: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    item: PropTypes.any.isRequired,
    position: PropTypes.number.isRequired,
    handleSetPosition: PropTypes.func.isRequired,
    handleFetchData: PropTypes.func.isRequired,
    updateOperationId: PropTypes.string.isRequired,
    listOperationId: PropTypes.string.isRequired,
    deleteOperationId: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    itemComponent: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    style: PropTypes.any.isRequired,
  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const {
      connectDragSource, isDragging, connectDropTarget, isOver,
      style, itemComponent: ItemComponent, ...props
    } = this.props
    return (
      connectDragSource(connectDropTarget((
        <div
          style={{
            opacity: isDragging ? 0.5 : 1,
            cursor: 'move',
          }}
        >
          <GridListTile style={{ ...style }} >
            <ItemComponent {...props} />
          </GridListTile>
        </div>
      )))
    )
  }
}
