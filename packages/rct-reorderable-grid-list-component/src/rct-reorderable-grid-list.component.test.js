import React from 'react'
import ReactDOM from 'react-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import RctReorderableGridListComponent from './rct-reorderable-grid-list.component'

const debug = buildDebug('react-packages:packages:test:rct-reorderable-grid-list-component')

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctReorderableGridListComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

const testProps = {
  cols: 3,
  handleSetPosition: () => {},
  handleFetchData: () => {},
  updateOperationId: 'userChatboxUpdate',
  deleteOperationId: 'userChatboxDelete',
  listOperationId: 'userChatboxList',
  data: [],
  itemComponent: <div />,
}

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <RctReorderableGridListComponent {...testProps} />
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount(<RctReorderableGridListComponent {...testProps} />)
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctReorderableGridListComponent {...testProps} />)
  expect(wrapper).toMatchSnapshot()
})
