import { observable, action, computed, runInAction } from 'mobx'
import moment from 'moment-timezone'
import buildDebug from 'debug'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'
import datePickerStore from '@4geit/rct-date-picker-store'

const debug = buildDebug('react-packages:packages:rct-event-list-store')

export class RctEventListStore {
  @observable inProgress = false
  @observable page = 0
  @observable data = []
  @computed get sortedData() {
    debug('sortedData()')
    /* eslint-disable no-param-reassign */
    const group = this.data.reduce((obj, item) => {
      const { startTime, timeZone } = item
      const date = moment(startTime).tz(timeZone).format('YYYY-MM-DD')
      obj[date] = obj[date] || []
      obj[date].push(item)
      return obj
    }, Object.create(null))
    /* eslint-enable no-param-reassign */
    return Object.entries(group).map(([date, timeslots]) => ({
      date,
      timeslots: timeslots.slice().sort((a, b) => moment(a.startTime).diff(moment(b.startTime), 'minutes')),
    }))
  }
  static getNumberOfWeeks(start, end) {
    debug('getNumberOfWeeks()')
    const endDate = moment(end)
    const startDate = moment(start)
    return endDate.diff(startDate, 'week')
  }
  static getNextEvent(start, week, day) {
    debug('getNextEvent()')
    const startTimeWeek = moment(start).add(week, 'week')
    return moment(startTimeWeek).day(day + 1).tz('UTC').format('YYYY-MM-DDTHH:mm:ss.000\\Z')
  }
  @action appendData(value) {
    debug('appendData()')
    this.data = this.data || []
    this.data = this.data.concat(value)
    this.page += 1
    debug(this.data)
  }
  @action reset() {
    debug('reset()')
    this.data = []
    this.page = 0
  }
  @action async fetchData({ operationId }) {
    debug('fetchData()')
    this.inProgress = true
    try {
      const { body: { list } } = await swaggerClientStore.client.apis.Account[operationId]({
        pageSize: 10,
        page: this.page,
        // start date range
        'dateRange[]': (datePickerStore.date && datePickerStore.date.format('YYYY-MM-DD')) || '',
        // disabled since the end date range is not working on the API
        // 'dateRange[]': (datePickerStore.date && datePickerStore.date.format('YYYY-MM-DD'))
        //   || moment().month(moment().month() + 1).format('YYYY-MM-DD'),
        // active status only
        status: 'active',
      })
      if (list && list.length) {
        const toAdd = []
        const { startTime: lastStartTime } = list.slice(-1)
        list.forEach((timeslot) => {
          const {
            eventId, activity, events, title, startTime, timeZone, daysRunning,
          } = timeslot
          if (!daysRunning.length) {
            toAdd.push(timeslot)
            return
          }
          const numberOfWeeks = this.constructor.getNumberOfWeeks(startTime, lastStartTime)
          Array.from(Array(numberOfWeeks).keys()).forEach((week) => {
            daysRunning.forEach((day) => {
              if (!events.length) {
                const newStartTime = this.constructor.getNextEvent(startTime, week, day)
                toAdd.push({
                  eventId, title, timeZone, startTime: newStartTime, activity, events,
                })
              }
              const isMatching = events.some(({
                eventInstanceId, title: innerTitle,
                timeZone: innerTimeZone, startTime: innerStartTime,
              }) => {
                const calcEventInstanceId = `${eventId}_${moment(startTime).days(day).tz('UTC').format('YYYYMMDDTHHmmss\\Z')}`
                if (eventInstanceId === calcEventInstanceId) {
                  toAdd.push({
                    eventId,
                    title: innerTitle,
                    timeZone: innerTimeZone,
                    startTime: innerStartTime,
                    activity,
                    events,
                  })
                  return true
                }
                return false
              })
              if (!isMatching) {
                toAdd.push(timeslot)
              }
            })
          })
        })
        runInAction(() => {
          debug(toAdd)
          this.appendData(toAdd)
          this.inProgress = false
        })
      }
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctEventListStore()
