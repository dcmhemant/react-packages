import { observable, action, reaction, runInAction } from 'mobx'
import buildDebug from 'debug'

import notificationStore from '@4geit/rct-notification-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

const debug = buildDebug('react-packages:packages:rct-common-store')

export class RctCommonStore {
  @observable appName = 'Test'
  @observable token = window && window.localStorage && window.localStorage.getItem('token')
  @observable user
  @observable isLoggedIn = false
  @observable appLoaded = false

  constructor() {
    debug('constructor()')
    reaction(
      () => this.token,
      (token) => {
        if (token) {
          window.localStorage.setItem('token', token)
        } else {
          window.localStorage.removeItem('token')
        }
      },
    )
  }

  @action setToken(token) {
    debug('setToken()')
    this.token = token
    this.isLoggedIn = true
  }
  @action setAppLoaded() {
    debug('setAppLoaded()')
    this.appLoaded = true
  }
  @action logout() {
    debug('logout()')
    window.localStorage.removeItem('token')
    this.token = undefined
    this.isLoggedIn = false
  }
  @action setUser(user) {
    debug('setUser()')
    this.user = user
  }
  @action async restoreSession(accountOperationId) {
    debug('restoreSession()')
    this.inProgress = true
    try {
      if (!this.token) {
        this.inProgress = false
        return
      }
      await swaggerClientStore.buildClientWithToken({ token: this.token })
      const { client: { apis: { Account } } } = swaggerClientStore
      const {
        body: {
          firstname, lastname, email: emailField, password: passwordField, company, address, phone,
        },
      } = await Account[accountOperationId || 'account']()
      runInAction(() => {
        this.isLoggedIn = true
        this.setUser({
          firstname, lastname, email: emailField, password: passwordField, company, address, phone,
        })
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctCommonStore()
