import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observable, computed, action, runInAction } from 'mobx'
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Paper from 'material-ui/Paper'
import Grid from 'material-ui/Grid'
import Button from 'material-ui/Button'
import Snackbar from 'material-ui/Snackbar'
import { CircularProgress } from 'material-ui/Progress'

import { RctNotificationStore } from '@4geit/rct-notification-store'

import ToolbarComponent from './toolbar.component'
import TableComponent from './table.component'
import Column from './column'

import './rct-data-table.component.css'

const debug = buildDebug('react-packages:packages:rct-data-table-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  root: {
    margin: 'auto',
    height: '100vh',
  },
  // TBD
}))
@withWidth()
@inject('swaggerClientStore', 'notificationStore')
@observer
export default class RctDataTableComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    swaggerClientStore: PropTypes.any.isRequired,
    notificationStore: PropTypes.instanceOf(RctNotificationStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    title: PropTypes.string,
    listOperationId: PropTypes.string.isRequired,
    addOperationId: PropTypes.string.isRequired,
    updateOperationId: PropTypes.string.isRequired,
    deleteOperationId: PropTypes.string.isRequired,
    bulkAddOperationId: PropTypes.string.isRequired,
    defaultColumns: PropTypes.arrayOf(PropTypes.string),
    // TBD
  }
  static defaultProps = {
    title: 'Data Table',
    defaultColumns: [],
    // TBD
  }

  async componentWillMount() {
    debug('componentWillMount()')
    await this.fetchData()
  }

  @computed get enabledColumns() {
    debug('enabledColumns()')
    return this.columns.filter(i => i.enabled)
  }
  @computed get mutableColumns() {
    debug('mutableColumns()')
    return this.columns.filter(i => !(['_id', 'id', 'user'].includes(i.name)))
  }

  @action setColumns(value) {
    debug('setColumns()')
    this.columns = value.map(({ name, enabled, type }) => new Column({ name, enabled, type }))
  }
  @action async fetchData() {
    debug('fetchData()')
    const {
      swaggerClientStore, notificationStore,
      listOperationId, defaultColumns,
    } = this.props
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body, headers } = await Account[listOperationId]({
        page: this.page + 1,
        pageSize: this.perPage,
      })
      runInAction(() => {
        if (body.length) {
          const [first] = body
          this.setColumns((
            Object.entries(first)
              .map(([key, value]) => ({
                name: key,
                // eslint-disable-next-line no-bitwise
                enabled: !!~defaultColumns.indexOf(key),
                type: typeof value,
              }))
          ))
          debug(body)
          debug(headers)
          this.data = body
          this.totalCount = parseInt(headers['x-totalcount'], 10)
        }
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async removeItem({ id }) {
    debug('removeItem()')
    const { swaggerClientStore, notificationStore, deleteOperationId } = this.props
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[deleteOperationId || 'productDelete']({
        id,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async addItem({ body }) {
    debug('addItem()')
    const { swaggerClientStore, notificationStore, addOperationId } = this.props
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[addOperationId || 'productAdd']({
        body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async importItem({ body }) {
    debug('importItem()')
    const { swaggerClientStore, notificationStore, bulkAddOperationId } = this.props
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[bulkAddOperationId || 'productBulkAdd']({
        body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async editItem({ id, body }) {
    debug('editItem()')
    const { swaggerClientStore, notificationStore, updateOperationId } = this.props
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[updateOperationId || 'productUpdate']({
        id,
        body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }

  @observable inProgress = false
  @observable columns = []
  @observable data = []
  @observable totalCount = 0
  @observable page = 0
  @observable perPage = 10
  @observable selected = []
  @observable removeAllSnackbarOpen = false
  @observable removeAllIds = []
  @observable removeSnackbarOpen = false
  @observable removeId = ''

  handleOnAddSubmit = async (fields) => {
    debug('handleOnAddSubmit()')
    await this.addItem({ body: fields })
    await this.fetchData()
  }
  handleOnImportSubmit = async (importedData) => {
    debug('handleOnImportSubmit()')
    await this.importItem({ body: importedData.slice() })
    await this.fetchData()
  }
  handleOnEditSubmit = ({ id }) => async (fields) => {
    debug('handleOnEditSubmit()')
    await this.editItem({ id, body: fields })
    await this.fetchData()
  }
  @action handleOnRemoveAllClick = () => {
    debug('handleOnRemoveAllClick()')
    this.removeAllSnackbarOpen = true
    this.removeAllIds = this.selected
    this.selected = []
  }
  handleOnRemoveClick = ({ id }) => action(() => {
    debug('handleOnRemoveClick()')
    this.removeSnackbarOpen = true
    this.removeId = id
  })
  @action handleRemoveAllSnackbarClose = async (event, reason) => {
    debug('handleRemoveAllSnackbarClose()')
    if (reason === 'timeout') {
      debug(`remove all ids ${this.removeAllIds}`)
      await Promise.all(this.removeAllIds.map(async (removeId) => {
        await this.removeItem({ id: removeId })
      }))
      await this.fetchData()
    }
    runInAction(() => {
      this.removeAllSnackbarOpen = false
      this.removeAllIds = []
    })
  }
  @action handleRemoveSnackbarClose = async (event, reason) => {
    debug('handleRemoveSnackbarClose()')
    if (reason === 'timeout') {
      debug(`remove id ${this.removeId}`)
      await this.removeItem({ id: this.removeId })
      await this.fetchData()
    }
    runInAction(() => {
      this.removeSnackbarOpen = false
      this.removeId = ''
    })
  }
  handleOnSelectChange = ({ id }) => action(() => {
    debug('handleOnSelectChange()')
    const selectedIndex = this.selected.indexOf(id)
    let newSelected = []
    if (selectedIndex === -1) { // when index not selected yet, add it
      newSelected = newSelected.concat(this.selected.slice(), id)
    } else if (selectedIndex === 0) { // when index is the first item
      newSelected = newSelected.concat(this.selected.slice(1))
    } else if (selectedIndex === this.selected.length - 1) { // when index is the last item
      newSelected = newSelected.concat(this.selected.slice(0, -1))
    } else if (selectedIndex > 0) { // when index is between
      newSelected = newSelected.concat(
        this.selected.slice(0, selectedIndex),
        this.selected.slice(selectedIndex + 1),
      )
    }
    this.selected = newSelected
  })
  @action handleOnSelectAllChange = (event, checked) => {
    debug('handleOnSelectAllChange()')
    debug(`checked: ${checked}`)
    if (checked) {
      this.selected = this.data.map(({ id }) => id)
      return
    }
    this.selected = []
  }
  @action handleOnChangePage = async (event, page) => {
    debug('handleOnChangePage()')
    this.page = page
    await this.fetchData()
  }
  @action handleOnChangeRowsPerPage = async ({ target: { value } }) => {
    debug('handleOnChangeRowsPerPage()')
    this.perPage = value
    await this.fetchData()
  }

  render() {
    debug('render()')
    const { title, classes } = this.props

    if (this.inProgress) {
      return (
        <Grid container justify="center" alignItems="center" className={classes.root}>
          <Grid item xs style={{ textAlign: 'center' }}>
            <CircularProgress />
          </Grid>
        </Grid>
      )
    }

    return (
      <div>
        <Paper elevation={0}>
          <ToolbarComponent
            title={title}
            columns={this.mutableColumns.slice()}
            onAddSubmit={this.handleOnAddSubmit}
            onImportSubmit={this.handleOnImportSubmit}
            onRemoveAllClick={this.handleOnRemoveAllClick}
          />
          <TableComponent
            data={this.data.slice()}
            totalCount={this.totalCount}
            page={this.page}
            perPage={this.perPage}
            columns={this.mutableColumns}
            enabledColumns={this.enabledColumns}
            removed={this.removeAllIds.slice()}
            removeId={this.removeId}
            selected={this.selected.slice()}
            onRemoveClick={this.handleOnRemoveClick}
            onSelectChange={this.handleOnSelectChange}
            onSelectAllChange={this.handleOnSelectAllChange}
            onEditSubmit={this.handleOnEditSubmit}
            onChangePage={this.handleOnChangePage}
            onChangeRowsPerPage={this.handleOnChangeRowsPerPage}
          />
        </Paper>
        <Snackbar
          open={this.removeSnackbarOpen}
          autoHideDuration={5000}
          message={<span>Item removed!</span>}
          onRequestClose={this.handleRemoveSnackbarClose}
          action={[
            <Button key="undo" color="accent" dense onClick={this.handleRemoveSnackbarClose}>Undo</Button>,
          ]}
        />
        <Snackbar
          open={this.removeAllSnackbarOpen}
          autoHideDuration={5000}
          message={<span>Items removed!</span>}
          onRequestClose={this.handleRemoveAllSnackbarClose}
          action={[
            <Button key="undo" color="accent" dense onClick={this.handleRemoveAllSnackbarClose}>Undo</Button>,
          ]}
        />
        { this.data && !this.data.length && (
          <Snackbar
            open
            autoHideDuration={5000}
            message={<span>There is no data to display!</span>}
          />
        )}
      </div>
    )
  }
}
