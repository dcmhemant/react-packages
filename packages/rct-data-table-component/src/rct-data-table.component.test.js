import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

import RctDataTableComponent from './rct-data-table.component'

const debug = buildDebug('react-packages:packages:test:rct-data-table-component')

const stores = {
  commonStore,
  swaggerClientStore,
  notificationStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctDataTableComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

const testProps = {
  title: 'Contact List',
  listOperationId: 'contactList',
  deleteOperationId: 'contactDelete',
  addOperationId: 'contactAdd',
  updateOperationId: 'contactUpdate',
  bulkAddOperationId: 'contactBulkAdd',
  enabledColumns: ['firstname', 'lastname', 'company'],
}

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores} >
      <RctDataTableComponent {...testProps} />
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <Provider {...stores} >
//       <RctDataTableComponent {...testProps} />
//     </Provider>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctDataTableComponent {...stores} {...testProps} />)
  expect(wrapper).toMatchSnapshot()
})
