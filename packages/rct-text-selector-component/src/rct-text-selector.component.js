/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { observable, action } from 'mobx'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'
import Typography from 'material-ui/Typography'
import Paper from 'material-ui/Paper'
import Highlightable from 'highlightable'

import './rct-text-selector.component.css'

const debug = buildDebug('react-packages:packages:rct-text-selector-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  sample: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    height: '100vh',
  }),
  selections: theme.mixins.gutters({
    paddingTop: 50,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    height: '100vh',
    backgroundColor: '#f1f1f1',
  }),
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctTextSelectorComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    sample: PropTypes.string.isRequired,
    // TBD
  }
  static defaultProps = {
    // TBD
  }

  @observable ranges = []

  handleOnTextHighlighted = action((range) => {
    debug('handleOnTextHighlighted()')
    debug(range)
    this.ranges.push(range)
    debug(this.ranges.slice())
  })

  render() {
    debug('render()')
    const { classes, title, sample } = this.props
    return (
      <Grid container spacing={0} direction="row" alignItems="stretch">
        {/* sample */}
        <Grid item xs>
          <Paper classes={{ root: classes.sample }} elevation={0} square>
            <Typography type="headline" gutterBottom>{ title }</Typography>
            <Highlightable
              ranges={this.ranges.slice()}
              enabled
              onTextHighlighted={this.handleOnTextHighlighted}
              highlightStyle={{
                backgroundColor: '#ffcc80',
              }}
              text={sample}
            />
          </Paper>
        </Grid>
        {/* selections */}
        <Grid item xs={6} sm={4} md={3} lg={2}>
          <Paper classes={{ root: classes.selections }} elevation={0} square>
            <Typography type="title" gutterBottom>User Selections:</Typography>
            <Grid container direction="column">
              { this.ranges.map(({ text }, index) => (
                <Grid item key={index}>
                  <Typography type="subheading">Selection {index + 1}:</Typography>
                  <Typography type="body1">{text}</Typography>
                </Grid>
              )) }
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}
