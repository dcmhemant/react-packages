# @4geit/rct-chatbox-list-store [![npm version](//badge.fury.io/js/@4geit%2Frct-chatbox-list-store.svg)](//badge.fury.io/js/@4geit%2Frct-chatbox-list-store)

---

store class to handle chatbox list component observables

## Demo

A live storybook is available to see how the store looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-chatbox-list-store*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-chatbox-list-store) package manager using the following command:

```bash
npm i @4geit/rct-chatbox-list-store --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-chatbox-list-store
```

2. Depending on where you want to use the store you will need to import the class instance `chatboxListStore` or inject it to your project JS file.

If you are willing to use it within a component, then you must use the `inject` decorator provided by `mobx-react` library.

For instance if you want to use this store in your `App.js` component, you can use the RctChatboxListStore store in the JSX code as follows:

```js
import React, { Component } from 'react'
import { inject } from 'mobx-react'
// ...
@inject('chatboxListStore')
class App extends Component {
  handleClick() {
    this.props.chatboxListStore.setVar1('dummy value')
  }

  render() {
    return (
      <div className="App">
        <button onClick={ this.handleClick.bind(this) } >Click here</button>
      </div>
    )
  }
}
```

If you are willing to use the class instance inside another store class, then you can just import the instance as follows:

```js
import chatboxListStore from '@4geit/rct-chatbox-list-store'

class DummyStore {
  @action doSomething() {
    chatboxListStore.setVar1('dummy value')
  }
}
```
