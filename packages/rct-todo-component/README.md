# @4geit/rct-todo-component [![npm version](//badge.fury.io/js/@4geit%2Frct-todo-component.svg)](//badge.fury.io/js/@4geit%2Frct-todo-component)

---

list

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-todo-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-todo-component) package manager using the following command:

```bash
npm i @4geit/rct-todo-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-todo-component
```

2. Depending on where you want to use the component you will need to import the class `RctTodoComponent` to your project JS file as follows:

```js
import RctTodoComponent from '@4geit/rct-todo-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctTodoComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctTodoComponent from '@4geit/rct-todo-component'
// ...
const App = () => (
  <div className="App">
    <RctTodoComponent />
  </div>
)
```

## AngularJS

`rct-todo-component` also supports integration in an AngularJS app, therefore you can call the AngularJS component directive into your AngularJS app and it will appears properly, you can have more details about its AngularJS integration [here](./src/index.ng.js).

Here are the steps you need to follow to get the component working on your AngularJS app:

* install the component thanks to npm as shown above
* add the following tag to the main `index.html` file of your app:

```html
<script src="node_modules/@4geit/rct-ng-vendor/dist/index.ng.js"></script>
<script src="node_modules/@4geit/rct-todo-component/dist/index.ng.js"></script>
```

* inject the component AngularJS module to your app module as below:

```js
angular
  .module('app', ['rct-todo-component'])
  // ...
```

* use the component directive where ever you want to add the component in your app as follow:

```html
<rct-todo-component></rct-todo-component>
```
