import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import BottomNavigation, { BottomNavigationButton } from 'material-ui/BottomNavigation'
import Icon from 'material-ui/Icon'

import './rct-footer.component.css'

const debug = buildDebug('react-packages:packages:rct-footer-component')

export const FooterItem = ({ icon, label }) => (
  <BottomNavigationButton
    label={label}
    value={label}
    icon={<Icon>{ icon }</Icon>}
  />
)
FooterItem.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  root: {
    width: 500,
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctFooterComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    children: PropTypes.arrayOf(FooterItem).isRequired,
  }
  static defaultProps = {
    // TBD
  }

  state = {
    value: 'Recents',
  }

  handleChange = (event, value) => {
    debug('handleChange()')
    this.setState({ value })
  }

  render() {
    debug('render()')
    const { classes, children } = this.props
    const { value } = this.state

    return (
      <BottomNavigation
        value={value}
        onChange={this.handleChange}
        className={classes.root}
      >
        { children }
      </BottomNavigation>
    )
  }
}
