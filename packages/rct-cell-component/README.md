# @4geit/rct-cell-component [![npm version](//badge.fury.io/js/@4geit%2Frct-cell-component.svg)](//badge.fury.io/js/@4geit%2Frct-cell-component)

---

cell to render each pixel of the conway's board

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-cell-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-cell-component) package manager using the following command:

```bash
npm i @4geit/rct-cell-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-cell-component
```

2. Depending on where you want to use the component you will need to import the class `RctCellComponent` to your project JS file as follows:

```js
import RctCellComponent from '@4geit/rct-cell-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctCellComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctCellComponent from '@4geit/rct-cell-component'
// ...
const App = () => (
  <div className="App">
    <RctCellComponent/>
  </div>
)
```
